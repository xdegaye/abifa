Features
========

``abifa`` creates a Makefile according to values of environment variables and
provides the following features through the use of Makefile targets:

* cross-compile cpython_ for Android
* install python (the result of the build) on an Android emulator
* run python as a command or interactively on this emulator
* run the ``buildbottest`` target of the cpython Makefile

A docker image on `Docker hub`_ can be used instead of installing all the
tools required by abifa as explained in the ``Installation on Linux`` section
of the documentation. The ``Docker`` section describes how to run and
optionally how to build an abifa docker image.

Documentation at `GitLab Pages`_.

.. _`GitLab Pages`: https://xdegaye.gitlab.io/abifa/
.. _`cpython`: https://www.python.org/
.. _`Docker hub`: https://hub.docker.com/r/xdegaye/abifa

.. vim:sts=2:sw=2:tw=78
