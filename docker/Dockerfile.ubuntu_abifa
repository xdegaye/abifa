# libncurses5 & libtinfo5 are included in the ubuntu:bionic image.
# The Android SDK requires libncurses5 and libtinfo5.
FROM ubuntu:bionic

LABEL title="Ubuntu for abifa"
LABEL home=https://gitlab.com/xdegaye/abifa
LABEL dockerfile=Dockerfile.ubuntu_abifa

# On ubuntu bionic the default java version is 10 and sdkmanager fails with:
# java.lang.NoClassDefFoundError: javax/xml/bind/annotation/XmlSchema
ARG JAVA_VERSION=8

# md5sum is part of coreutils.
# Required by the emulator:
#   libgl1-mesa-dev
#   libpulse-dev
#   qemu-kvm (needed only on the x86_64 architecture)
RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y \
    bash \
    build-essential \
    libffi-dev \
    curl \
    openjdk-$JAVA_VERSION-jre-headless \
    findutils \
    gawk \
    git \
    libgl1-mesa-dev \
    libpulse-dev \
    libreadline-dev \
    qemu-kvm \
    sed \
    tzdata \
    unzip \
    vim \
    zip \
    && rm -rf /var/lib/apt/lists/*

# Run as user 'pydev'.
RUN groupadd pydev && useradd --no-log-init -m -g pydev pydev
USER pydev:pydev
ENV HOME=/home/pydev
WORKDIR $HOME
