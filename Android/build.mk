# Android/build.mk

# Directory names.
py_version := $(shell cat $(PYTHON_SRC_PATH)/configure | \
                sed -e "s/^PACKAGE_VERSION=['\"]*\([0-9]*\.[0-9]*\)['\"]*$$\|^.*$$/\1/" -e "/^$$/d")
export BUILD_DIR := $(MAKEFILE_DIR)build
export DIST_DIR := $(MAKEFILE_DIR)dist
avd_dir := $(MAKEFILE_DIR)avd
native_build_dir := $(BUILD_DIR)/python-native-build
native_install_dir := $(BUILD_DIR)/python-native
py_name := python$(py_version)
export BUILD_TYPE := android-$(ANDROID_API)-$(ANDROID_ARCH)
py_host_dir := $(BUILD_DIR)/$(py_name)-$(BUILD_TYPE)
export PY_EXTDIR := $(BUILD_DIR)/$(py_name)-extlibs-$(BUILD_TYPE)
PY_DESTDIR := $(BUILD_DIR)/$(py_name)-install-$(BUILD_TYPE)
abiflags = $(shell echo $$(cat $(py_host_dir)/Makefile | \
    sed -e "s/^ABIFLAGS[= \t]*\(.*\)[ \t]*$$\|^.*$$/\1/" -e "/^$$/d"))
py_fullname = python$(py_version)$(abiflags)
export STDLIB_DIR := lib/python$(py_version)


ROOT_MAKE := $(MAKE) -f Makefile-$(BUILD_TYPE)

# Target variables names.
python := $(py_host_dir)/python
config_status := $(py_host_dir)/config.status
export PYTHON_ZIP := $(DIST_DIR)/$(py_name)-$(BUILD_TYPE).zip
export PY_STDLIB_ZIP := $(DIST_DIR)/$(py_name)-$(BUILD_TYPE)-stdlib.zip


# Rules.
build: $(python)
dist: $(PYTHON_ZIP) $(PY_STDLIB_ZIP)
required: $(native_install_dir)/bin/python prefixes

include $(abifa_srcdir)/Android/emulator.mk

$(native_build_dir)/config.status: $(PYTHON_SRC_PATH)/configure
	@# Run distclean upon the switch to a new Python release.
	@# 'Touch' the 'Makefile' target first to prevent a useless run of configure.
	@cur=$$(cat $(PYTHON_SRC_PATH)/configure | sed -e "s/^VERSION=\([0-9]\+\.[0-9]\+\)$$\|^.*$$/\1/" -e "/^$$/d"); \
	prev=$$cur; \
	if test -f "$(native_build_dir)/python"; then \
	    prev=$$($(native_build_dir)/python -c "from sys import version_info; print('.'.join(str(i) for i in version_info[:2]))"); \
	fi; \
	if test "$$prev" != "$$cur"; then \
	    echo "---> Cleanup before the switch from Python version $$prev to $$cur"; \
	    $(MAKE) -C $(native_build_dir) -t Makefile; \
	    $(MAKE) -C $(native_build_dir) distclean; \
	fi
	@echo "---> Run the native configure script."
	mkdir -p $(native_build_dir)
	cd $(native_build_dir); $(PYTHON_SRC_PATH)/configure --prefix=$(native_install_dir)

$(native_install_dir)/bin/python: $(native_build_dir)/config.status
	@echo "---> Build the native interpreter."
	$(MAKE) -C $(native_build_dir) install
	cd $(native_install_dir)/bin && ln -sf python3 python

# Target-specific exported variables.
build host python_dist setup hostclean config_site: \
                    export PATH := $(native_install_dir)/bin:$(PATH)
external_libraries: export CC := $(CC)
external_libraries: export AR := $(AR)
external_libraries: export READELF := $(READELF)
external_libraries: export CFLAGS := $(CFLAGS)
external_libraries: export LDFLAGS := $(LDFLAGS)
external_libraries: export ANDROID_ARCH := $(ANDROID_ARCH)
external_libraries:
	mkdir -p $(BUILD_DIR)/external-libraries
ifdef WITH_LIBFFI
	$(MAKE) -C $(abifa_srcdir)/Android/external-libraries libffi
endif
ifdef WITH_NCURSES
	$(MAKE) -C $(abifa_srcdir)/Android/external-libraries ncurses
endif
ifdef WITH_READLINE
	$(MAKE) -C $(abifa_srcdir)/Android/external-libraries readline
endif
ifdef WITH_SQLITE
	$(MAKE) -C $(abifa_srcdir)/Android/external-libraries sqlite
endif

openssl:
ifdef WITH_OPENSSL
	mkdir -p $(BUILD_DIR)/external-libraries
	$(MAKE) -C $(abifa_srcdir)/Android/external-libraries -f Makefile.openssl
endif

$(config_status): $(makefile) $(PYTHON_SRC_PATH)/configure
	mkdir -p $(py_host_dir)
	@echo "---> Run configure for $(BUILD_TYPE)."
	cd $(py_host_dir); \
	    PYTHON_SRC_PATH=$(PYTHON_SRC_PATH) \
	    PKG_CONFIG_PATH=$(PY_EXTDIR)/$(SYS_EXEC_PREFIX)/lib/pkgconfig \
	    CPPFLAGS="$$CPPFLAGS -I$(PY_EXTDIR)/$(SYS_EXEC_PREFIX)/include" \
	    LDFLAGS="$$LDFLAGS -L$(PY_EXTDIR)/$(SYS_EXEC_PREFIX)/lib" \
	    $(abifa_srcdir)/Android/configure-android \
	    $(config_args)

utilities: $(native_install_dir)/bin/python prefixes external_libraries openssl

$(python): utilities
	CONFIG_SITE=$(MAKEFILE_DIR)config-$(ANDROID_API)-$(ANDROID_ARCH).site \
	    $(ROOT_MAKE) $(config_status)
	$(ROOT_MAKE) host

config_site: $(native_install_dir)/bin/python prefixes
	@rm -f $(config_status)
	$(ROOT_MAKE) LDFLAGS="$$LDFLAGS -pie" $(config_status)
	@rm -f $(config_status)
	$(ROOT_MAKE) _emulator
	/bin/sh $(PYTHON_SRC_PATH)/Misc/build_config_site.sh \
	    $(abifa_srcdir)/Android/tools/adb_push_exec.sh \
	    $(py_host_dir) config-$(ANDROID_API)-$(ANDROID_ARCH).site
	$(ROOT_MAKE) kill_emulator

prefixes:
	$(eval PREFIXES := $(shell cd $(abifa_srcdir)/Android/tools; $(python_cmd) \
	    -c 'from android_utils import parse_prefixes; parse_prefixes("$(config_args)")'))
	$(eval export SYS_PREFIX := $(word 1, $(PREFIXES)))
	$(eval export SYS_EXEC_PREFIX := $(word 2, $(PREFIXES)))
	@echo "---> Prefixes are $(SYS_PREFIX), $(SYS_EXEC_PREFIX)."

disabled_modules: setup_local := $(py_host_dir)/Modules/Setup.local
disabled_modules:
	echo "*disabled*" > $(setup_local)
	echo "_uuid" >> $(setup_local)
	echo "grp" >> $(setup_local)
	echo "_crypt" >> $(setup_local)

host: disabled_modules
	@echo "---> Build Python for $(BUILD_TYPE)."
	@if test -f $(config_status); then \
	    $(MAKE) -C $(py_host_dir) all; \
	else \
	    echo "Error: please run 'make config', missing $(config_status)"; \
	    false; \
	fi

# Hack to fix a pip bug looping infinitely on SELinux platforms that restrict
# access to hard links. See issue https://github.com/pypa/pip/issues/5322.
pip_hack:
	@echo "---> Workaround issue bpo-31046 (ensurepip does not honour the value of 'prefix')."
	@if test -d $(PY_DESTDIR)/usr/local; then \
	    cp $(PY_DESTDIR)/usr/local/bin/pip3 $(PY_DESTDIR)/$(SYS_EXEC_PREFIX)/bin/pip; \
	    cp -r $(PY_DESTDIR)/usr/local/lib/$(py_name)/site-packages \
	        $(PY_DESTDIR)/$(SYS_PREFIX)/lib/$(py_name); \
	fi

	@echo "---> Install pip hack."
	@pip_lockfile_py=$(PY_DESTDIR)/$(SYS_PREFIX)/lib/$(py_name)/site-packages/pip/_vendor/lockfile/__init__.py; \
	    if test -f $$pip_lockfile_py; then \
	        echo "from . import mkdirlockfile" >> $$pip_lockfile_py; \
	        echo "LockFile = mkdirlockfile.MkdirLockFile" >> $$pip_lockfile_py; \
	    else \
	        echo "The pip hack is not needed."; \
	    fi

python_dist: $(python)
	@echo "---> Install Python for $(BUILD_TYPE)."
	$(MAKE) DESTDIR=$(PY_DESTDIR) -C $(py_host_dir) install > make_install.log
	cp --no-dereference $(PY_EXTDIR)/$(SYS_EXEC_PREFIX)/lib/*.so* $(PY_DESTDIR)/$(SYS_EXEC_PREFIX)/lib
	chmod u+w $(PY_DESTDIR)/$(SYS_EXEC_PREFIX)/lib/*.so*
	tdir=$(SYS_EXEC_PREFIX)/share/terminfo/l; mkdir -p $(PY_DESTDIR)/$$tdir && \
	    cp $(PY_EXTDIR)/$$tdir/linux $(PY_DESTDIR)/$$tdir
	if test -d "$(PY_EXTDIR)/$(SYS_EXEC_PREFIX)/etc"; then \
	    cp -r $(PY_EXTDIR)/$(SYS_EXEC_PREFIX)/etc $(PY_DESTDIR)/$(SYS_EXEC_PREFIX); \
	fi
	$(ROOT_MAKE) pip_hack

$(PYTHON_ZIP): python_dist
	@echo "---> Zip the machine-specific Python library."
	mkdir -p $(DIST_DIR)
	rm -f $(PYTHON_ZIP)

	cd $(PY_DESTDIR)/$(SYS_EXEC_PREFIX)/bin; \
	    ln -sf python3 python

	mkdir -p $(PY_DESTDIR)/$(SYS_EXEC_PREFIX)/etc; \
	    cp $(abifa_srcdir)/Android/resources/inputrc $(PY_DESTDIR)/$(SYS_EXEC_PREFIX)/etc

	cd $(PY_DESTDIR)/$(SYS_EXEC_PREFIX); \
	    $(STRIP) bin/python$(py_version); \
	    chmod 755 lib/*.so*; find . -type f -name "*.so*" -print0 | xargs -0 $(STRIP)

	@# Remove the first '/' to avoid unzip warnings and an exit code of '1'.
	cd $(PY_DESTDIR); export SYS_EXEC_PREFIX=$${SYS_EXEC_PREFIX#/}; \
	    zip -g $(PYTHON_ZIP) $$SYS_EXEC_PREFIX/bin/python3 \
	        $$SYS_EXEC_PREFIX/bin/python $$SYS_EXEC_PREFIX/bin/python$(py_version) \
	        $$SYS_EXEC_PREFIX/bin/pip; \
	    zip -rg $(PYTHON_ZIP) \
	        $$SYS_EXEC_PREFIX/include/$(py_fullname)/pyconfig.h \
	        $$SYS_EXEC_PREFIX/$(STDLIB_DIR)/ \
	        $$SYS_EXEC_PREFIX/share/terminfo/l/linux \
	        $$SYS_EXEC_PREFIX/etc/ \
	        -x \*failed.so

	# Zip the shared libraries excluding symlinks.
	cd $(PY_DESTDIR); export SYS_EXEC_PREFIX=$${SYS_EXEC_PREFIX#/}; \
	    libs=""; \
	    for l in $$SYS_EXEC_PREFIX/lib/*.so*; do \
	        test ! -h $$l && libs="$$libs $$l"; \
	    done; \
	    zip -g $(PYTHON_ZIP) $$libs

$(PY_STDLIB_ZIP): python_dist
	@echo "---> Zip the Python library."
	mkdir -p $(DIST_DIR)
	rm -f $(PY_STDLIB_ZIP)
	cd $(PY_DESTDIR); export SYS_PREFIX=$${SYS_PREFIX#/}; \
	    zip -Drg $(PY_STDLIB_ZIP) $$SYS_PREFIX/$(STDLIB_DIR)* \
	        -x \*.so \*.pyo \*opt-\*.pyc

# Build a wheel for a third-party extension module running the following
# command from the package source directory:
#     make -f /path/to/Makefile/generated/by/makesetup setup
setup: required
	$(native_python_exe) -m pip install --upgrade pip
	$(native_python_exe) -m pip install wheel
	PYTHON_PROJECT_BASE=$(py_host_dir) \
	    $(native_python_exe) setup.py bdist_wheel;

# Make things clean, before making a distribution.
distclean: kill_emulator hostclean
	rm -f $(PYTHON_ZIP)
	rm -f $(PY_STDLIB_ZIP)
	rm -f $(DIST_DIR)/*.sh
	-rmdir $(DIST_DIR)
	rm -f $(BUILD_DIR)/python
	rm -rf $(PY_DESTDIR)

hostclean:
	-$(MAKE) -C $(py_host_dir) distclean

# Remove everything for the given ANDROID_API and ANDROID_ARCH except the avd.
clean: distclean
	rm -rf $(BUILD_DIR)/python-native
	rm -rf $(py_host_dir)
	rm -rf $(BUILD_DIR)/external-libraries/*-$(BUILD_TYPE)
	-rmdir $(BUILD_DIR)/external-libraries
	rm -rf $(PY_EXTDIR)
	-rmdir $(BUILD_DIR)
	-rmdir $(avd_dir)
	rm -rf $(DIST_DIR)/gdb/android-$(ANDROID_API)-$(ANDROID_ARCH)
	-rmdir $(DIST_DIR)/gdb
	-rmdir $(DIST_DIR)
	rm Makefile Makefile-android-$(ANDROID_API)-$(ANDROID_ARCH)


# Tool names.
wget := $(shell which wget 2>/dev/null)
curl := $(shell which curl 2>/dev/null)
ifeq (curl, $(findstring curl, $(curl)))
    export DOWNLOAD := $(curl) -O
else ifeq (wget, $(findstring wget, $(wget)))
    export DOWNLOAD := $(wget)
else
    $(warning *** Cannot download the external libraries with wget or curl, \
      please download those libraries to the 'Android/external-libraries' \
      directory.)
endif

.PHONY: build prefixes disabled_modules host \
        external_libraries openssl setup config_site \
        dist python_dist distclean hostclean clean utilities pip_hack
