# Android/build-config

ndk_version=19

# Set default values if not defined.
: ${ANDROID_NDK_ROOT:=$HOME/android/android-ndk}
: ${ANDROID_SDK_ROOT:=$HOME/android/android-sdk}
: ${ANDROID_API:=24}
: ${ANDROID_ARCH:=x86_64}
: ${TESTTIMEOUT:=1200}

build_config_error ()
{
    echo "Error: $1" >&2
    exit 1
}

! test -d "$ANDROID_NDK_ROOT" &&
    build_config_error "\$ANDROID_NDK_ROOT ($ANDROID_NDK_ROOT) is not a directory"
version=$(cat "$ANDROID_NDK_ROOT/source.properties" | \
    sed -n 's/^.*Pkg.Revision[ \t]*=[ \t]*\([0-9]\+\).*$/\1/p')
test "$version" != $ndk_version &&
    build_config_error "The installed NDK version is $version but version $ndk_version is required."
unset ndk_version version

test $((ANDROID_API)) -lt 21 && \
    build_config_error "\$ANDROID_API ($ANDROID_API) must be 21 or greater"
test $((ANDROID_API)) -lt 24 && test "$ANDROID_ARCH" = arm64 && \
    build_config_error "Cannot build or run arm64 at an API level less than 24"

# No NDK has been released at SDK API level 22, use NDK API 21 instead.
test "$ANDROID_API" = 22 && ANDROID_API=21

if test -n "$PYTHON_SRC_PATH"; then
    ANDROID_BUILD=$($PYTHON_SRC_PATH/config.guess);
else
    build_config_error "The environment variable PYTHON_SRC_PATH is not defined"
fi

case "$ANDROID_ARCH" in
    armv7)
        ANDROID_HOST=armv7a-linux-androideabi
        target=arm-linux-androideabi
        APP_ABI=armeabi-v7a
        ;;
    arm64)
        ANDROID_HOST=aarch64-linux-android
        target=$ANDROID_HOST
        APP_ABI=arm64-v8a
        ;;
    x86)
        ANDROID_HOST=i686-linux-android
        target=$ANDROID_HOST
        APP_ABI=x86
        ;;
    x86_64)
        ANDROID_HOST=x86_64-linux-android
        target=$ANDROID_HOST
        APP_ABI=x86_64
        ;;
    *)
        build_config_error "ANDROID_ARCH must be set to armv7, arm64, x86 or x86_64."
        ;;
esac

machine=$(uname -m)
kernel=$(uname -s | tr A-Z a-z)
TOOLCHAIN="$ANDROID_NDK_ROOT/toolchains/llvm/prebuilt/${kernel}-${machine}"

# Build variables.
CC="$TOOLCHAIN/bin/$target$ANDROID_API-clang"
AR="$TOOLCHAIN/bin/$target-ar"
READELF="$TOOLCHAIN/bin/$target-readelf"
STRIP="$TOOLCHAIN/bin/$target-strip --strip-debug --strip-unneeded"
CFLAGS="$CFLAGS -fPIC"
unset machine kernel target

ccache -V >/dev/null 2>&1 && { CC="ccache $CC"; CFLAGS="$CFLAGS -Wno-parentheses-equality"; }
