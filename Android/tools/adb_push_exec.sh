#! /bin/sh
#
#   usage: adb_push_exec.sh /path/to/executable
#
# Use adb to push an executable on an Android emulator or device and
# to run the executable.

# The name of the executable using variable substitution instead of basename.
executable=${1##*/}

adb=$ANDROID_SDK_ROOT/platform-tools/adb
tmpdir=/data/local/tmp

$adb push $1 $tmpdir >/dev/null 2>&1
$adb shell $tmpdir/$executable
status=$?
$adb shell rm $tmpdir/$executable
exit $status
