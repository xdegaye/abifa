#!/usr/bin/env bash

echo "Waiting for the emulator to start..."

bootanim=""
count=0
max_count=360

until [[ "$bootanim" =~ "stopped" || \
         "$bootanim" =~ "running" && ! "$bootanim" =~ "not running" ]]; do
    # Run the adb command.
    bootanim=$($@ 2>&1)
    let "count += 1"
    echo -n "  emulator starting up"
    echo " - bootanim: $bootanim ($count:$max_count)"
    if [[ $count -gt max_count ]]; then
        echo "Timeout exceeded; failed to start the emulator"
        exit 1
    fi
    sleep 1
done
echo "  emulator ready"

