Release history
===============

Version 1.1
-----------

* Update to NDK version r19.

Version 1.0
-----------

* Initial release.
