.. abifa documentation master file, created by
   sphinx-quickstart on Tue Feb 12 19:54:08 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Features
========

``abifa`` creates a Makefile according to values of
:ref:`Environment-variables` and provides the following features through the
use of :ref:`Makefile-targets`:

* cross-compile cpython_ for Android
* install python (the result of the build) on an Android emulator
* run python as a command or interactively on this emulator
* run the ``buildbottest`` target of the cpython Makefile

A docker image on `Docker hub`_ can be used instead of installing all the
tools required by abifa as explained in the :ref:`Installation-on-Linux`
section of the documentation. The :ref:`Docker` section describes how to
run and optionally how to build an abifa docker image.

.. _`cpython`: https://www.python.org/
.. _`Docker hub`: https://hub.docker.com/r/xdegaye/abifa

.. vim:sts=2:sw=2:tw=78

.. toctree::
   :hidden:
   :caption: Table of content
   :maxdepth: 3

   Home <self>
   targets
   docker
   linux
   history

.. vim:sts=2:sw=2:tw=78
