.. _Docker:

Docker
======

Requirements
------------

* An x86_64 linux platform with docker installed.

* KVM virtualization support must be enabled when running an image for the
  x86_64 architecture. KVM is used by the Android emulator (based on QEMU) and
  enables virtualization at nearly native speeds.

  KVM virtualization is usually provided through packages by the linux
  distribution. It may also be necessary to enable virtualization by updating
  the BIOS.

* The download size of the ``abifa:r19-24-x86_64-master`` image from the
  `Docker hub`_ is about 2 GB (downloaded only once, the first time the image
  is used) and the size of the image itself is 9 GB. The writable layer of
  the container that is run from this image is 2.4 GB when the emulator has
  been started and python installed on the emulator. So the minimum size
  required to run abifa using docker is 11.4 GB.


abifa containers
----------------

Run buildbottest
^^^^^^^^^^^^^^^^^

The ``buildbottest`` target of the cpython Makefile is run when the abifa
image is run as a container without a ``COMMAND`` command line argument::

  $ docker run -it --privileged xdegaye/abifa:r19-24-x86_64-master

This command processes the following tasks:

* pull the ``abifa:r19-24-x86_64-master`` image from the `Docker hub`_ and
  start the container
* pull the latest changes from the GitHub cpython repository
* cross-compile python and create a distribution
* start the emulator and install the distribution on the emulator
* run the ``buildbottest`` target of the cpython Makefile
* stop the container

The ``--privileged`` option is necessary here to enable KVM virtualization
when running the emulator for the x86_64 architecture and not needed for the
other architectures (armv7, arm64).

Run bash
^^^^^^^^

A bash shell in the container is run when the abifa image is run with the bash
``COMMAND`` command line argument::

  $ docker run -it --privileged xdegaye/abifa:r19-24-x86_64-master bash

One enters a bash shell on ubuntu-bionic in the container as user pydev, in
the HOME directory of this user and with everything setup and ready to run the
:ref:`Makefile-targets`:

* Android NDK and SDK installed
* abifa Makefile created
* native python built
* external libraries cross-compiled
* AVD created
* cpython and abifa repositories cloned as shallow clones (at the time of the
  abifa image creation)

The first target to be run in this shell must be the ``install`` target (see
:ref:`Makefile-targets` for the description of all the targets) with the
following command run in pydev HOME::

  pydev@<container id>:~$ make install

The container stops when you exit the bash shell. It can be re-started later
with the command::

  $ docker start -i <container id>

Develop python
^^^^^^^^^^^^^^

Use a ``bind mount`` to debug and fix issues in python and, instead of using
the GitLab cpython shallow repository built into the image, use a local
repository.

Here is an example with a readonly bind mount of a local repository located at
PYTHON_SRC_PATH::

  $ docker run -it --privileged \
      --mount type=bind,source=$PYTHON_SRC_PATH,target=/home/pydev/cpython,readonly \
      abifa:r19-24-x86_64-master

The python source tree must be clean as for all builds of python made out of
the source tree. So if this is not the case, run ``make -C $PYTHON_SRC_PATH
clean`` on the host before running ``make install`` in the container.


abifa images
------------

The name of an abifa tag follows this convention:

``<NDK version>-<API level>-<architecture>-<cpython branch>``

Instead of using one of the images from the `Docker hub`_, one can build the
same image from the Dockerfile at abifa `GitLab repository`_ with the
command::

  $ docker build --tag=abifa:r19-24-x86_64-master \
      https://gitlab.com/xdegaye/abifa.git#master:docker

Configure the build
^^^^^^^^^^^^^^^^^^^^

The Dockerfile uses ``ARG`` directives to allow building different images.
For example a new image can be built for a different API level (here API 21)
with::

  $ docker build --tag=abifa:r19-21-x86_64-master --build-arg ANDROID_API=21
      https://gitlab.com/xdegaye/abifa.git#master:docker

Or the arm64 image can be built with::

  $ docker build --tag=abifa:r19-24-arm64-master
      --build-arg ANDROID_ARCH=arm64 --build-arg APP_ABI=arm64-v8a
      https://gitlab.com/xdegaye/abifa.git#master:docker


.. _`Docker hub`: https://hub.docker.com/r/xdegaye/abifa
.. _`GitLab repository`: https://gitlab.com/xdegaye/abifa/tree/master/docker

.. vim:sts=2:sw=2:tw=78
