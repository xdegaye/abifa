Makefile targets
================

- The ``Android/makesetup`` script creates a Makefile and its command line
  arguments are passed directly to Python ``configure``. Although the
  ``--prefix`` command line argument may be omitted, it does not make much sense
  as ``/usr/local`` does not exist on Android. One may run ``makesetup`` for
  multiple (api, architecture) combinations in the same directory.

  The targets of this Makefile allow to:

  - Cross-compile Python with external libraries.
  - Build a distribution to be copied on an Android device on an existing
    Android application.
  - Run the Python interpreter on the Android emulator using an adb_ shell, the
    interpreter runs with readline enabled.
  - Run a Python command on the Android emulator.
  - Start a gdb remote debugging session of a python process running on the
    emulator.
  - Run the buildbottest.


.. _Makefile-targets:

Build targets
--------------
*config_site*
  * Create the ``config-$(ANDROID_API)-$(ANDROID_ARCH).site`` file in the
    current directory. This file is used by ``configure``, when
    cross-compiling Python, to set the correct cache values for the current
    API and architecture instead of using default pessimistic values. It needs
    to be created only once for each (API, architecture) tuple. Using this
    target assumes that ``PR 12429`` has been merged into the current branch
    of the Python repository being used.

*build*
  * Compile the native Python interpreter and cross-compile the external
    libraries and python. This is the default target.

*dist*
  * Make a distribution consisting of:

    - The machine-specific Python library zip file.
    - The Python standard library zip file.

*distclean*
  * Kill the emulator if it is running.
  * Make things clean, before making a distribution.
  * Removes the DESTDIR directory where the external libraries have been
    copied and where the cross-compiled Python has been installed by the
    command ``make DESTDIR=$(PY_DESTDIR) install`` run on Python own Makefile.
    The external libraries will not be rebuilt on the next make invocation.

*clean*
  Remove everything for the given (api, architecture) except the AVD.


Emulator targets
----------------

*install*
    Make a distribution, create the AVD if it does not exist [1]_, start the
    emulator after having wiped out the previous data on the AVD and install
    the content of the two zip files on the emulator. Then start an adb_ shell
    (see the ``adb_shell`` target description below). The ``install`` target
    fails if there is already an emulator running. Run the ``kill_emulator``
    target in this case.

    When the AVD does not exist, it is mandatory to run this target first, as
    the other targets rely on tools installed on the emulator by ``install``.

    When the AVD is created, it is not necessary to answer the following
    question printed on the screen at that time::

      Do you wish to create a custom hardware profile? [no]

*adb_shell*
    Start the emulator if not already running and create an adb_ shell on the
    emulator.

    At the first shell prompt a message is printed giving the shell command to
    run to source a shell script that sets the environment variables needed to
    run the Python interpreter. The script mainly does:

    - Set ``PATH`` and ``LD_LIBRARY_PATH``.
    - Set ``HOME`` to ``sys.exec_prefix``.
    - Set miscellaneous stuff such as the terminal type, the terminal width and
      the readline inputrc configuration file.
    - Change the current directory to ``$HOME``.

    After sourcing this script one can run the Python interpreter.

*buildbottest*
    Start the emulator if not already running and run on the emulator the
    ``buildbottest`` target of the cpython Makefile.

*pythoninfo*
    Start the emulator if not already running and run the ``test.pythoninfo``
    module on the emulator.

*python*
    Start the emulator if not already running and run the python command defined
    by ``PYTHON_ARGS``.  This variable is set on make command line or as an
    environment variable when make is run. Quotes in the command are interpreted
    both by the shell when interpreting the make command line and by make
    itself, so they must be escaped properly such as in this example::

        $ make python PYTHON_ARGS="-c 'print(\\\"Hello world.\\\")'"

*kill_emulator*
    Kill the emulator. Useful when the emulator refuses to be shutdown from its
    GUI or when there is no GUI.

*avdclean*
    Remove the AVD. This is the proper way to remove an AVD, do not just remove
    the corresponding directory in the avd/ directory because Android maintains
    also some information on the AVD in  ~/.android/avd.

*gdb*
    Start a gdb remote debugging session of a python process running on the
    emulator. There must be a unique python process running on the emulator.

    This requires that Python 2 is installed on the build platform and that
    ``python2`` is found in the ``$PATH``.

    The debugging session can be customized with the following variables set
    on the command line (or as environment variables) to the value ``yes``
    (for example ``GDB_PYTHON=yes make gdb``):

    - ``GDB_PYTHON=yes``
        Import the `libpython module`_ in gdb and get detailed information of
        the PyObject(s) at the cost of speed.

    - ``GDB_LOGGING=yes``
        Setup logging in gdb and have the output of all the gdb commands also
        redirected to ./gdb.log.

    - ``GDB_SIGILL=yes``
        Work around the problem that gdb fails with SIGILL in
        ``__dl_notify_gdb_of_libraries()`` whenever a library is loaded when
        debugging on the armv7 platforms.

.. [1] Android Virtual Device. This is the image run by the emulator. It is
   specific to each (api, architecture) and it holds also the configuration of
   the emulator.

.. _adb: https://developer.android.com/studio/command-line/adb.html
.. _`libpython module`: https://github.com/python/cpython/blob/master/Tools/gdb/libpython.py

.. vim:sts=2:sw=2:tw=78
